package rand

import (
	"fmt"
	"math/rand"
	"strings"

	"gitlab.com/thorchain/bepswap/common"
)

var BnBTickers = []common.Ticker{
	"BNB",
	"RUNE-B1A",
}

func RandTicker() common.Ticker {
	n := rand.Int() % len(BnBTickers)
	return BnBTickers[n]
}

func RandAmount(low, high float64) common.Amount {
	f := randNum(low, high)
	return common.NewAmountFromFloat(f)
}

func RandCoin(low, high float64) common.Coin {
	return common.NewCoin(
		RandTicker(),
		RandAmount(low, high),
	)
}

func RandTxID() common.TxID {
	txID, _ := common.NewTxID(RandString(64))
	return txID
}

func RandBnbAddress() common.BnbAddress {
	suffix := strings.ToLower(RandString(39))
	bnb, _ := common.NewBnbAddress(fmt.Sprintf("bnb%s", suffix))
	return bnb
}
