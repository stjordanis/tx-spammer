package types

import (
	"fmt"

	"gitlab.com/thorchain/bepswap/common"
)

type CreatePoolMemo struct {
	Ticker common.Ticker `json:"ticker"`
}

func NewCreatePoolMemo(ticker common.Ticker) CreatePoolMemo {
	return CreatePoolMemo{ticker}
}

func (m CreatePoolMemo) String() string {
	return fmt.Sprintf("CREATE:%s", m.Ticker.String())
}

type StakeMemo struct {
	Ticker common.Ticker `json:"ticker"`
}

func NewStakeMemo(ticker common.Ticker) StakeMemo {
	return StakeMemo{ticker}
}

func (m StakeMemo) String() string {
	return fmt.Sprintf("STAKE:%s", m.Ticker.String())
}

type UnStakeMemo struct {
	Ticker common.Ticker `json:"ticker"`
	Amount common.Amount `json:"amount"`
}

func NewUnStakeMemo(ticker common.Ticker, amt common.Amount) UnStakeMemo {
	return UnStakeMemo{ticker, amt}
}

func (m UnStakeMemo) String() string {
	return fmt.Sprintf("WITHDRAW:%s:%s", m.Ticker.String(), m.Amount.String())
}

type SwapMemo struct {
	Ticker      common.Ticker     `json:"ticker"`
	Destination common.BnbAddress `json:"destination"`
}

func NewSwapMemo(tick common.Ticker, des common.BnbAddress) SwapMemo {
	return SwapMemo{tick, des}
}

func (m SwapMemo) String() string {
	return fmt.Sprintf(
		"SWAP:%s:%s",
		m.Ticker.String(),
		m.Destination.String(),
	)
}
