package types

import (
	"strings"

	"gitlab.com/thorchain/bepswap/common"
)

const (
	DEXHost = "testnet-dex.binance.org"
)

type Transaction struct {
	DEXHost string            `json:"dex_host"`
	From    common.BnbAddress `json:"from"`
	To      common.BnbAddress `json:"to"`
	Coins   common.Coins      `json:"coins"`
	Memo    string            `json:"memo"`
	Key     string            `json:"key"`
}

func NewTransaction(from, to common.BnbAddress, coins common.Coins, memo, key string) Transaction {
	return Transaction{
		From:  from,
		To:    to,
		Coins: coins,
		Memo:  memo,
		Key:   key,
	}
}

func (t Transaction) String() string {
	sb := strings.Builder{}
	sb.WriteString("From:" + t.From.String())
	for _, c := range t.Coins {
		sb.WriteString("denom:" + c.Denom.String())
		sb.WriteString("Amount:" + c.Amount.String())
	}
	sb.WriteString("Memo: %s" + t.Memo)
	return sb.String()
}
