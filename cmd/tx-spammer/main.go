package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/spf13/cobra"

	"gitlab.com/thorchain/bepswap/common"
	randTx "gitlab.com/thorchain/tx-spammer/rand"
	"gitlab.com/thorchain/tx-spammer/shipper"
	"gitlab.com/thorchain/tx-spammer/types"
)

func getVersion() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Print the version number of tx-spammer",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("v0.1")
		},
	}
}

func getGenerateCmd() *cobra.Command {
	generateCmd := &cobra.Command{
		Use:   "generate",
		Short: "Generate random transactions",
		Run: func(cmd *cobra.Command, args []string) {

			// Figure out our output medium
			shipString, _ := cmd.Flags().GetString("output")
			ship, _ := shipper.StringToShipper(shipString)
			var output shipper.Shipper
			switch ship {
			case shipper.ShipStdOut:
				output = shipper.NewStdOut()
			case shipper.ShipBinance:
				output = shipper.NewBinance()
			default:
				panic("Invalid output")
			}

			low, _ := cmd.Flags().GetFloat64("low")
			high, _ := cmd.Flags().GetFloat64("high")

			flagFrom, _ := cmd.Flags().GetString("from")
			from, _ := common.NewBnbAddress(flagFrom)
			flagTo, _ := cmd.Flags().GetString("to")
			to, _ := common.NewBnbAddress(flagTo)
			var fromInstance common.BnbAddress
			if to.IsEmpty() {
				panic("Must supply valid to address (--to)")
			}

			limit, _ := cmd.Flags().GetInt("limit")
			interval, _ := cmd.Flags().GetInt("interval")
			privateKey, _ := cmd.Flags().GetString("key")

			// iterate over each ticker and create a pool
			coin := common.NewCoin("BNB", "0.0001")
			for _, ticker := range randTx.BnBTickers {
				if from.IsEmpty() {
					fromInstance = randTx.RandBnbAddress()
				} else {
					fromInstance = from
				}
				memo := types.NewCreatePoolMemo(ticker)
				tx := types.NewTransaction(
					fromInstance,
					to,
					common.Coins{coin},
					memo.String(),
					privateKey,
				)

				_ = output.Send(tx)
			}

			ticker := time.NewTicker(time.Duration(interval) * time.Millisecond)
			i := 0
			func() {
				for _ = range ticker.C {
					if from.IsEmpty() {
						fromInstance = randTx.RandBnbAddress()
					} else {
						fromInstance = from
					}

					coin := randTx.RandCoin(low, high)

					var memo string
					n := rand.Int() % 3
					switch n {
					case 0:
						memo = types.NewStakeMemo(coin.Denom).String()
					case 1:
						amt := randTx.RandAmount(1, 30)
						memo = types.NewUnStakeMemo(coin.Denom, amt).String()
					case 2:
						memo = types.NewSwapMemo(coin.Denom, "").String()
					}

					tx := types.NewTransaction(
						fromInstance,
						to,
						common.Coins{coin},
						memo,
						privateKey,
					)

					_ = output.Send(tx)

					i += 1
					if i >= limit && limit > 0 {
						break
					}
				}
			}()
		},
	}

	generateCmd.Flags().StringP("output", "o", "stdout", "Destination to send the transaction (stdout, binance, or statechain)")
	generateCmd.Flags().IntP("limit", "l", -1, "Limit number of transactions sent")
	generateCmd.Flags().Float64P("low", "", 0.1, "Low coin amount")
	generateCmd.Flags().Float64P("high", "", 0.8, "High coin amount")
	generateCmd.Flags().StringP("from", "f", "", "BNB from address")
	generateCmd.Flags().StringP("key", "k", "", "BNB from address private key")
	generateCmd.Flags().StringP("to", "t", "", "BNB to address")
	generateCmd.Flags().StringP("statechain", "s", "http://localhost:1317", "Statechain address to send mock transactions to")
	generateCmd.Flags().IntP("interval", "i", 300, "Interval to generate a transaction in milliseconds")
	return generateCmd
}

func main() {
	rootCmd := &cobra.Command{
		Use:   "tx-spammer",
		Short: "Create random binance transactions",
	}

	// Construct Root Command
	rootCmd.AddCommand(
		getVersion(),
		getGenerateCmd(),
	)

	err := rootCmd.Execute()
	if err != nil {
		panic(err)
	}
}
