package shipper

import (
	"log"

	"gitlab.com/thorchain/tx-spammer/types"
)

type StdOut struct {
}

func NewStdOut() StdOut {
	return StdOut{}
}

func (ship StdOut) Send(tx types.Transaction) error {
	log.Printf("%s", tx.String())
	return nil
}
