package shipper

import (
	"gitlab.com/thorchain/bepswap/common"
	. "gopkg.in/check.v1"
	"os"
	"testing"

	"gitlab.com/thorchain/tx-spammer/types"
)

func Test(t *testing.T) { TestingT(t) }

type BinanceTestSuite struct{}

var (
	_       = Suite(&BinanceTestSuite{})
	toAddr  = common.BnbAddress("tbnb10tedug0w95yvlhg9fqeyj7rzzda4y3y2w9wrp3")
	privKey = os.Getenv("PRIV_KEY_TEST")
)

func (BinanceTestSuite) TestBinance(c *C) {
	if privKey == "" {
		c.Log("No private key set!")
		c.Fail()
	}

	var coins common.Coins
	coin := common.Coin{Denom: "BNB", Amount: "0.1"}
	coins = append(coins, coin)
	b := NewBinance()

	// Invalid Private Key
	err := b.Send(types.Transaction{To: toAddr, Key: "", Memo: "MEMO", Coins: coins})
	c.Assert(err, NotNil)

	// Invalid to Address
	err = b.Send(types.Transaction{To: "", Key: privKey, Memo: "MEMO", Coins: coins})
	c.Assert(err, NotNil)

	// No tokens
	err = b.Send(types.Transaction{To: toAddr, Key: privKey, Memo: "MEMO"})
	c.Assert(err, NotNil)
}
