package shipper

import (
	"log"
	"strconv"

	"gitlab.com/thorchain/tx-spammer/types"

	sdk "github.com/binance-chain/go-sdk/client"
	"github.com/binance-chain/go-sdk/client/transaction"
	btypes "github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/keys"
	"github.com/binance-chain/go-sdk/types/msg"
)

type Binance struct {
}

func NewBinance() Binance {
	return Binance{}
}

func (ship Binance) Send(tx types.Transaction) error {
	keyManager, err := keys.NewPrivateKeyManager(tx.Key)
	if err != nil {
		log.Printf("Error: %s", err)
		return err
	}

	client, err := sdk.NewDexClient(types.DEXHost, btypes.TestNetwork, keyManager)
	if err != nil {
		log.Printf("Error: %s", err)
		return err
	}

	toAddr, _ := btypes.AccAddressFromBech32(string(btypes.AccAddress(tx.To)))
	var tokens btypes.Coins

	for _, c := range tx.Coins {
		amount, _ := strconv.ParseFloat(c.Amount.String(), 64)
		tokens = append(tokens, btypes.Coin{Denom: c.Denom.String(), Amount: int64(amount * 100000000)})
	}

	send, err := client.SendToken([]msg.Transfer{{toAddr, tokens}}, true, transaction.WithMemo(tx.Memo))

	if err != nil {
		log.Printf("Error: %v", err)
		return err
	}

	log.Printf("Send: %v", send)

	return nil
}
