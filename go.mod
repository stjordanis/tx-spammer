module gitlab.com/thorchain/tx-spammer

go 1.12

require (
	github.com/binance-chain/go-sdk v1.1.0
	github.com/spf13/cobra v0.0.5
	gitlab.com/thorchain/bepswap/common v0.0.0-20190816093251-b84c21cee45c
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127
)
